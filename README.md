# Diseño Bases de Datos

Materia de la Facultad de Informatica UNLP 2do Año

**Student:** Yamil Daza 👋

**LinkedIn:** https://www.linkedin.com/in/yamil-daza/ 🚀

## Ejercicios de "Diseño de Bases de Datos" - Practicas 🛠

### PRACTICA 1 - Modelo Conceptual:
   - Enunciados de practica [LINK](./Practica1/Practica1.pdf)

### PRACTICA 2 - Conceptual, Logico & Fisico: 
   - Enunciados de practica [LINK](./Practica2/DBD%202023%20-%20Práctica%202.pdf)

### PRACTICA 3 - Algebra Relacion: 
   - Enunciados de practica [LINK](./Practica3/DBD%202023-%20Práctica%203.docx)

### PRACTICA 4 - SQL: 
   - Enunciados de practica [LINK](./Practica4/DBD%202023%20-%20Práctica%204%20.docx.pdf)


